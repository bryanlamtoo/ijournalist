package com.creativeDNA.rumor.util;

import java.util.Comparator;

import com.creativeDNA.rumor.model.Category;

/**
 * This class is used to compare two <code>Category</code> objects by their Slug property.
 * @author |the creativeDNA|
 *
 */
public class CategoryIndexComparator implements Comparator<Category> {

	/**
	 * Compares two <code>Category</code> objects by their Slug property.
	 */
	@Override
	public int compare(Category c1, Category c2) {
		return c1.getSlug().compareTo(c2.getSlug());
	}

}
