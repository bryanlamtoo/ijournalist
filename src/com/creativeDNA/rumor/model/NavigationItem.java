package com.creativeDNA.rumor.model;



public class NavigationItem {
	private String mTitle;
	private int mIcon;
	
	
	public String getmTitle() {
		return mTitle;
	}
	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}
	public int getmIcon() {
		return mIcon;
	}
	public void setmIcon(int mIcon) {
		this.mIcon = mIcon;
	}
	
	

}
