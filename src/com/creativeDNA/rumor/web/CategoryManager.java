package com.creativeDNA.rumor.web;

import java.util.ArrayList;

import com.creativeDNA.rumor.model.Category;



public class CategoryManager {

	/* variables */
	ResourceInterface resourceInterface;
	boolean isLoading;
	volatile boolean noError;
	ArrayList<Category> categories;
	
	synchronized void setIsLoading(boolean keepLoading) {
		this.isLoading = keepLoading;
	}
	
	synchronized boolean isLoading() {
		return isLoading;
	}
	
	public CategoryManager(ResourceInterface service) {
		
		this.resourceInterface = service;
	}
	public void stopLoading() {
		noError = false;
		setIsLoading(false);
	}
	public void Fetch() {
		// Check that we are not already loading
		if (!isLoading()) {
			categories = new ArrayList<Category>();
			setIsLoading(true);
			noError = true;
			start();
		}
	}
	
	void start(){
		if (isLoading()) {
			System.out.println("Inside Manager, STARTING");
			//Fetch all categories
			new FetchAllCategories(this).execute();
		}
	}
	
	public void handleData(ArrayList<Category> result){
		this.categories = result;
		resourceInterface.categoriesLoaded(result.toArray(new Category[result.size()]));
	}

}
