package com.creativeDNA.rumor.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.creativeDNA.rumor.MainActivity;
import com.creativeDNA.rumor.R;
import com.creativeDNA.rumor.SplashActivity;

public final class IntroFragment extends Fragment {
    private static final String KEY_LAYOUT = "IntroFragment:Content";
    int mLayout;
    View layout;

    public static IntroFragment newInstance(int page) {
        IntroFragment fragment = new IntroFragment();
        fragment.mLayout = page;
       
    return fragment;
    }
    
    TextView mStart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_LAYOUT)) {
        	mLayout = savedInstanceState.getInt(KEY_LAYOUT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        switch(mLayout){
        case 0:
        	layout = inflater.inflate(R.layout.page_one, container, false);
        	break;
        case 1:
        	layout = inflater.inflate(R.layout.page_two, container, false);
        	break;
        case 2:
        	layout = inflater.inflate(R.layout.page_three, container, false);
        	mStart = (TextView) layout.findViewById(R.id.start);
        	mStart.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivity(new Intent(getActivity(), MainActivity.class));
				}
			});
        	break;
        }
        
        

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_LAYOUT, mLayout);
    }
}
